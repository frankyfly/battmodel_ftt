clc 
clear all 
close all 

%% build testsignal
T = 0.100;  % Sampling time in
Fs = 1/T; % Sampling frequency  
L = 1024; % Lenght 

U_ocv = 10; % Volt
Ri = 0.05;    % Ohm
I = 20; % Ampere

%% cinstanzs for testsignal
K1 = 0.1;
T1 = 1;
K2 = 0.02;
T2 = 8;
K3 = 0.01;
T3 = 300;

t = (0:L-1)*T;  % Time Vektor
U = zeros(3,L);

Uri = Ri*I ;
U(1,1:L) = K1*(1-exp(-(t/T1)));
U(2,1:L) = K2*(1-exp(-(t/T2)));
U(3,1:L) = K3*(1-exp(-(t/T3)));
Ukl = U_ocv - Uri -U(1,1:L) -U(2,1:L) -U(3,1:L);

%% plot Batt Voltage
figure('Name','Batt Voltage');
plot(1000*t(1:100),Ukl(1:100))
title('Voltage')
xlabel('t (milliseconds)')
ylabel('Ukl(t)')

%% FFT of Ubatt
n=10;
%Ufft= -(Ukl-U_ocv); %

Ufft= U_ocv-Ukl-Uri; %


Y = fft((Ufft),n);

P2 = abs(Y/n);
P1 = P2(1:n/2+1);
P1(2:end-1) = 2*P1(2:end-1);

f = Fs*(0:(n/2))/n;

figure('Name','FFT');
plot(f,P1,'-o') 
title('Single-Sided Amplitude Spectrum of U(t)')
xlabel('f (Hz)')
ylabel('|P1(f)|')

%% Rebuild Ubatt

Sri = Ri*I;
Skl = U_ocv -Sri;


for i = 1:1:n
   %S(i) = P2(i)*(1-exp(-(t/i)));
   S = P2(i)*(1-exp(-(t/i)));
   Skl = Skl -  S;
end

figure('Name','Sim Voltage');
plot(1000*t(1:100),Skl(1:100))
title('Voltage')
xlabel('t (milliseconds)')
ylabel('Ukl(t)')

U_err = Ukl-Skl;


figure('Name','Error Voltage');
plot(1000*t(1:100),U_err(1:100))
title('Voltage')
xlabel('t (milliseconds)')
ylabel('U_err(t)')















